variable "network" {
        type = map
        default ={

  "vpccidr" = "172.168.0.0/16"
  "name" = "ginuvpc"
  "subnet1cidr" = "172.168.0.0/18"
  "subnet1name" = "Publicsub1"
  "subnet2cidr" = "172.168.64.0/18"
  "subnet2name" = "Publicsub2"
  "subnet3cidr" = "172.168.192.0/18"
  "subnet3name" = "Publicsub3"
  "subnet4cidr" = "172.168.128.0/18"
  "subnet4name" = "Privatesub"
  "IGname"      = "ginuig"
  "Natname"     = "ginunatgw"
  "Routepublic" = "PublicRoute"
  "Routepubcidr" = "0.0.0.0/0"
  "Routeprivcidr" = "0.0.0.0/0"
  "Routepriv"    = "PrivateRoute"
        }
}

variable "security" {

        type = map
        default = {

"bastionsecname" = "allow_ssh"
"bastiontag"     = "allow_SSH_from_pub1"
"lb1name"        = "allow_HTTP_ELB_SERVER"
"lb1secname"     = "allow_HTTP_HTTPS_ELB"
"lb2name"        = "allow_HTTP_ELB2_SERVER"
"lb2secname"     = "allow_HTTP_HTTPS_ELB2"

        }
}
